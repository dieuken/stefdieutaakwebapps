var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  title: String,
  link: String,
  upvotes: {type: Number, default: 0},
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});


PostSchema.methods.upvote = function(cb) {
  this.upvotes += 1;
  this.save(cb);
};

mongoose.model('Post', PostSchema);
/*
Array.prototype.filter = function(func){
	var results = [];
	this.forEach(function(item){
		if(fuct(item)) results.push(item);
	})
	return results;
};

var arr = [-2, 5, 8];
arr.filter(function(item){
	return item > 0;
})*/