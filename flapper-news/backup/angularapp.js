var app = angular.module('flapperNews', ['ui.router']);

app.controller(
	'MainCtrl', 
	['$scope',
		function($scope, postFactory)
		{
			/*var o = {naam: 'jef'}
			console.dir(o)*/
  			//$scope.posts = ['post 1','post 2','post 3','post 4','post 5'];
  			$scope.posts = postfactory.posts;
  			// niet voor factory
        /*[
  				{title: 'post 1', upvotes: 5},
  				{title: 'post 2', upvotes: 3},
  				{title: 'post 3', upvotes: 2},
  				{title: 'post 4', upvotes: 6},
  				{title: 'post 5', upvotes: 1}
  			];*/

//submit in een form
  			$scope.addPost = function()
  			{
  				postFactory.create({
            title
          })
  				$scope.title = '';
  				$scope.link = '';
  			}

			$scope.incrementUpvotes = function(post)
  			{
  				postFactory.upvotes(post);
  			};

// voor button
/*
  			$scope.addPost = function()
  			{
  				$scope.posts.push(
  						{
  							title: 'new post',
  							upvotes: 0
  						}
  					)
  			}
*/
		}
	]
	);

// shift + enter voor multiline in console

app.factory('postFactory'[$http, function($http){
  var postFactory = {
    posts: []
  };
  postFactory.getAll = function(){
      $http/get('/posts').success(function(data){
        angular.copy(data, postFactory.posts);
      });
  };
  postFactory.create = function(post){
      $http.post('/posts', post).success(fuction(post){
        postFactory.posts.push(post);
      })
  }
  postFactory.upvote = function(post){
    return $http.put('/posts' + post.__id).then(function(res){
      return res.data;
    })
  }
  return postFactory;
}])

app.config([
  '$stateProvider','$urlRouteProvider',
  function($stateProvider, $urlRouteProvider)
  {
    $stateProvider
    .state('home',
    {
      url: '/home'
      templateUrl: '/home.html',
      controller: 'MainCtrl',
      resolve: (
          postPromise: ['PostFactory', function(postFactory){
            return postFactory.getAll();
          }])
    });
    .state('posts',
    {
      url: '/posts/{id}',
      templateUrl: '/posts.html',
      controller: 'PostCtrl'
    });

    $urlRouteProvider.otherwise('/home');
  }
]);

app.controller('MainCtrl', [
  '$scope', '$stateParams', 'postFactory',
  function($scope, $stateParams, postFactory)
  {
      $scope.post = postFactory.posts[$stateParams.id];
  }
])