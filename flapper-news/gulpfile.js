var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');

var plugins = gulpLoadPlugins();
//Variabele om de map aan te duiden waar de tests zitten
var testFolder = './test';

gulp.task('runTests', function(){
  return gulp.src(testFolder + '/*.js')
         .pipe(plugins.mocha());
});
